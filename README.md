# Etude comparative 2022 des plateformes Copernicus DIAS

| | | |
|-----------------|:-------------|:---------------:|
| ![Logo Connect by CNES](img/cnes.svg "Connect by CNES") | ![Logo IDGEO](img/idgeo.png "IDGEO") | ![Logo SOMEWARE](img/someware.png "SOMEWARE") |
| [https://www.connectbycnes.fr](https://www.connectbycnes.fr) | [https://www.idgeo.fr/](https://www.idgeo.fr/) | [https://www.someware.fr](https://www.someware.fr) |

**Table des matières**

[[_TOC_]]

## ​1.​ Objet de l'étude

Ce document propose une synthèse de travaux réalisés en 2019, 2020 et 2022 par la société [SOMEWARE](https://www.someware.fr) dans le cadre de plusieurs missions d&#39;étude comparative des plateformes Copernicus DIAS et accompagnement de sociétés utilisatrices de données spatiales.

Les travaux sur l'étude comparative, menés en 2019 et 2020, ont été réalisés pour le compte de l&#39;organisme de formation [IDGEO](https://www.idgeo.fr/) (financement par le Programme « [Connect by CNES](https://www.connectbycnes.fr))
La mission d'accompagnement de sociétés a été réalisée en 2022 et financée par le [CNES](https://www.cnes.fr)(Appel à candidatures DIAS Priming pour accompagner des entreprises vers l’utilisation des DIAS).

Les travaux menés en 2019 ont mené à l&#39;écriture d&#39;un premier rapport d&#39;étude comparatif es plateformes DIAS. Le présent rapport constitue une mise à jour de l&#39;étude 2019 pour tenir compte de toutes les évolutions opérées par les plateformes DIAS jusqu&#39;en septembre 2022.

**Pour rappel, les cinq plateformes DIAS ([CREODIAS](https://creodias.eu/), [MUNDI WEB SERVICES](https://mundiwebservices.com/), [ONDA](https://www.onda-dias.eu), [SOBLOO](https://sobloo.eu/), [WEKEO](https://www.wekeo.eu/)), ont été financées par la Commission Européenne dans le but de fournir :**

- **un accès centralisé aux données et informations du programme Copernicus (ainsi que d&#39;autres données spatiales et non spatiales),**
- **des services de traitement/calcul évitant aux développeurs la nécessité de télécharger des fichiers volumineux pour les traiter localement.**

Néanmoins la nature précise des services proposés par chaque DIAS, les protocoles sur lesquels ils s&#39;appuient, ainsi que les technologies et performances proposées sont globalement peu lisibles, et peu de documentation publique permet à ce jour de comparer de façon claire et nuancée les offres proposées par les différentes plateformes.

En conséquence, **le travail réalisé a eu pour objectif de dresser un panel aussi complet que possible de l&#39;offre de chaque plateforme en 2019 puis en 2020 et 2022, notamment dans le but d&#39;améliorer la formation CQP GEOMATICIEN (Développeur d&#39;applications spatiales) proposée par [IDGEO](https://www.idgeo.fr/).**

**Ce travail est le fruit d&#39;échanges avec les représentants de chaque plateforme, complété par le test des services proposés et l'étude de plusieurs cas d'usage. Ses constats se veulent aussi neutres que possible, sans objectif de désigner la meilleure plateforme DIAS.**

**Ce millésime 2022 de l'étude comparative des DIAS s'inscrit dans le contexte de la fin quasi-programmée des plateformes DIAS. Après plusieurs années de développement et financement par ka Commission Européenne, un nouvel appel d'offre de l'ESA, dont les lauréats seront connus à l'automne 2022, prône la mise en service d'une seule et unique plateforme européenne sous le nom DAS pour _Data Access Service_.**

Nous espérons que ce - très probable dernier - millésime de l'étude comparative des DIAS contribuera à une meilleure compréhension de l&#39;offre des plateformes, et qu'il éclairera plus généralement sur les fonctionnalités et caractéristiques importantes pour les usagers des plateformes de diffusion de données spatiales.


## 2. Abstract

This document provides a summary of work carried out in 2019, 2020 and 2022 by [SOMEWARE](https://www.someware.fr) as part of several study missions of the Copernicus DIAS platforms and a support mission for companies using space data.

The work on the comparative study was carried out in 2019 and 2020 on behalf of [IDGEO](https://www.idgeo.fr/) (financed by the [Connect by CNES] program (https://www.connectbycnes.fr))
The mission to advise several companies was carried out in 2022 and financed by the [CNES](https://www.cnes.fr)(DIAS Priming call for applications to advise companies towards the use of DIAS).

The work carried out in 2019 led to the writing of a first report comparing DIAS platforms. This report is an update of the 2019 study to take account of all the developments in the DIAS platforms up to September 2022.

**As a reminder, the five DIAS platforms ([CREODIAS](https://creodias.eu/), [MUNDI WEB SERVICES](https://mundiwebservices.com/), [ONDA](https://www.onda-dias.eu), [SOBLOO](https://sobloo.eu/), [WEKEO](https://www.wekeo.eu/)), were funded by the European Commission in order to provide :**

- **centralized access to data and information from the Copernicus Program (as well as other spatial and non-spatial data from other programs),**
- **processing / calculation services avoiding the need for developers to build their own computation infrastructure.**

However, the precise nature of the services offered by each DIAS, the protocols on which they are based, as well as the technologies and performances offered habe been to date difficult to read, and little public documentation allows a clear and neutral comparison of the different platforms.

As a result, **the work carried out tries to draw a panel of the offer of each platform, in particular with the aim of improving the CQP GEOMATICIEN (Application Developer) training offered by [IDGEO](https://www.idgeo.fr/).**

**This work is the result of discussions with the representatives of each platform, completed by the test of different services offered by each platform. Its findings are intended to be as neutral as possible, without the objective of designating the best DIAS platform.**

**This 2022 edition of the DIAS benchmarking study is taking place in the context of the near-programmed end of the DIAS platforms. After several years of development and funding by the European Commission, a new ESA call for tenders, the winners of which will be known in the autumn of 2022, advocates the implementation of a single European platform under the name DAS for Data Access Service.**

We hope that this - very likely final - edition of the DIAS benchmarking study will contribute to a better understanding of the platforms offer, and that it will shed light more generally on the functionalities and features that are important to users of spatial data distribution platforms.


## ​3.​ Méthodologie

**Entretiens**

L&#39;ensemble des entretiens réalisés en 2019 auprès des représentants de chaque plateforme a été conduit sur la base du questionnaire ci-dessous (non communiqué à l&#39;avance).

Les questions posées avaient pour objectif d&#39;aborder des problématiques larges (liste des services par exemple), souvent déjà bien décrites sur le portail de chaque DIAS mais permettant d&#39;amorcer la discussion, et discuter de points très précis comme les délais de mise à disposition de données…

Toutes les questions et réponses n&#39;ont pas nécessairement permis de dégager des éléments utiles pour l&#39;étude, et certains points ne sont pas mentionnés dans les pages qui suivent faute d&#39;avoir pu apporter une information pertinente.

Questionnaire:

- Quels services proposez-vous ?
- Quels jeux de données proposez-vous ?
- Quelle est la fréquence d&#39;ajout de jeux de données ?
- Est-ce que vous comptez proposer les jeux de données pour toute date, sans limite dans le temps ? Ou pensez-vous proposer seulement les jeux de données les plus récents ? Quelle est votre politique en la matière ?
- Réalisez-vous des opérations de transformation sur les données en amont des services de diffusion ? Si oui, lesquels ?
- Quelles opérations peut-on réaliser via vos services sur les données ?
- Qu&#39;est-ce que les services de calcul proposent ? Quel est le bénéfice par rapport à un calcul réalisé en dehors de votre infrastructure ?
- Quels systèmes d&#39;exploitation et bibliothèques sont proposés pour réaliser les calculs ? Quelles sont les méthodes de déploiement, debug, monitoring, logging du code de calcul ?
- Proposez-vous d&#39;autres types de services ? Par exemple, stockage, hébergement d&#39;applications...
- Quel est le pricing de chacun de vos services ?
- Quelle est la licence d&#39;utilisation des services ? Des données ? Qu&#39;incluent les droits d&#39;utilisation des services et données ?
- Quel est la politique en cas de changement sur un service ? Quel délai de prévenance ?
- Quelle est la politique de support/maintenance ?
- Quel est l&#39;uptime des services ?
- Utilisez-vous des protocoles standards pour servir de l&#39;imagerie, réaliser des calculs... ?
- Quelles sont les méthodes d&#39;authentification possibles ?
- Peut-on incorporer l&#39;appel aux services dans une page web ? Des pages sur des domaines différents ?
- Est-il possible depuis les services de calculs d&#39;accéder aux données via des moyens plus performants que les API Web (accès direct via Object Storage par exemple) ?
- Proposez-vous des outils/bibliothèques pour faciliter l&#39;utilisation de vos services ? Quelles sont leurs licences ? Est-il possible d&#39;accéder à leur code, de les modifier ?
- Proposez-vous une expertise pour utiliser au mieux vos services ? Quid notamment de la question des performances, cruciale pour l&#39;imagerie spatiale ?
- Quel est votre cahier des charges en termes d&#39;assurance-qualité de vos services et données ? Quelles règles vous imposez-vous ?
- Qu&#39;est-ce qui différencie votre offre de celle des autres DIAS ?
- Qu&#39;est-ce qui, selon vous, serait la meilleure méthode pour calculer une image NDVI en utilisant vos services ?

La mise à jour 2020 de ces entretiens, réalisée en septembre/octobre 2020, a quant à elle porté sur le questionnaire suivant :

- Quelles sont les nouveautés depuis l&#39;an dernier ? Fonctionnalités / Données disponibles à chaud/froid / Prix / Offre de calcul en ligne / Outils / Offre de service / Performances ?
- Pouvez-vous nous indiquer quelques références client ? Y a-t-il des applications publiques ou, du moins, sur lesquelles nous pouvons obtenir des informations ?
- Qu&#39;est-ce qui différencie votre offre de celle des autres DIAS ?
- Qu&#39;est-ce qui, selon vous, serait la meilleure méthode pour calculer une image NDVI en utilisant vos services ?

Enfin, la mise à jour 2022 des entretiens a porté sur des questions précises issues d'échanges avec les 4 sociétés accompagnées par Someware pour l'idenfication de plateformes DIAS susceptibles de répondre à leurs besoins. La synthèse des réponses formulées par les DIAS est intégrées à l'ensemble du présent document.

**Test des services de chaque plateforme**

Pour réaliser le test des services de chaque plateforme, en 2019 puis en 2020, la première phase a porté sur la mise au point d&#39;une chaîne de traitement d&#39;images, déclinée en cinq versions pour s&#39;appuyer au maximum sur les services de chacun des DIAS.

Le cas d&#39;usage retenu pour la chaîne est un calcul d&#39;indice de végétation moyen et médian NDVI sur quelques parcelles identifiées au préalable.

Pour chaque plateforme, l&#39;idée a été de mettre au point un script capable de fournir la valeur de NDVI moyen/médian de quelques parcelles en exploitant au maximum les fonctionnalités proposées par chaque plateforme (fourniture de données, calcul...).

Dans un second temps, une étude des performances proposées par chaque plateforme a été réalisée, que ce soit pour l&#39;accès externe aux données ou interne (à l&#39;intérieur du cloud de chaque plateforme). Cette étude s&#39;appuie elle aussi sur du code produit pour chaque plateforme et cherchant à mesurer les temps d&#39;accès aux données en exploitant les meilleures fonctionnalités de service de données proposées par chaque DIAS.

Ces benchmarks ont été sensiblement améliorés en 2020 pour intégrer un comparatif de toutes les API de téléchargement interne des plateformes, ce que l&#39;étude 2019 n&#39;avait pas permis de réaliser de façon aussi large.

## ​4.​ Généralités

### ​4.1.​ Le programme Copernicus

Copernicus est le nom d&#39;un programme européen de surveillance de la Terre, auparavant nommé GMES (Global Monitoring for Environment and Security). Il s&#39;agit d&#39;une initiative conjointe de l&#39;ESA (Agence spatiale européenne) et, au travers de l&#39;Agence européenne pour l&#39;environnement (AEE), de l&#39;Union européenne, qui vise à doter l&#39;Europe d&#39;une capacité opérationnelle et autonome d&#39;observation de la Terre en tant que services d&#39;intérêt général européen, à accès libre, plein et entier.

Plus concrètement, Copernicus vise à rationaliser l&#39;utilisation de données relatives à l&#39;environnement et à la sécurité issues de sources multiples, afin de disposer d&#39;informations et de services fiables chaque fois que cela est nécessaire. En d&#39;autres termes, Copernicus permet de rassembler l&#39;ensemble des données obtenues à partir de satellites environnementaux et d&#39;instruments de mesure sur site, afin de produire une vue globale et complète de l&#39;état de notre planète.

Les domaines couverts par Copernicus sont vastes, et comprennent (liste non exhaustive) :

- l&#39;évolution des teneurs atmosphériques en aérosols et gaz à effet de serre
- le suivi de couche d&#39;ozone, et le taux d&#39;ultraviolet
- la climatologie, prévisions de l&#39;état de la mer, sécurité maritime, suivi du trafic maritime et de certaines pollutions marines (marées noires, dégazages...)
- la mesure, le contrôle et la gestion du développement urbain (urbanisation, périurbanisation...).
- la montée du niveau des océans
- l&#39;alerte aux aléas climatiques et catastrophes naturelles (tempêtes, inondations, sécheresse, fortes pluies, tremblements de terre, tsunamis, connaissance et suivi des inondations et des feux de forêt pour leur meilleure gestion…)
- la surveillance de l&#39;environnement, de l&#39;agriculture, des forêts, de la déforestation et de leurs conséquences
- l&#39;anticipation, alerte et gestion de catastrophes humanitaires (déplacements de population, migration humaine, camps de réfugiés, séquelles des guerres…)
- la sécurité civile, organisation des secours
- la sécurité et surveillance des frontières
- la lutte contre les trafics (par exemple de bois, de drogue…) et contre la piraterie en mer,
- la surveillance de zones (marines notamment) isolées ou provisoirement isolées.
- la disponibilité ou surexploitation de ressources naturelles.

#### Capteurs

Pour mener à bien ses missions, Copernicus s&#39;appuie en premier lieu sur la technologie spatiale et les moyens au sol des pays membres du programme. De nombreuses missions contribuent au programme :

- satellites optiques haute résolution : Pléiades, Deimos-2, RapidEye, SPOT HRS,
- satellites optiques basse résolution : SPOT VGT, PROBA-V,
- satellites radar : COSMO-Skymed, TerraSAR-X, Tandem-X, RadarSat,
- satellites radar altimétriques : CryoSat, Jason
- satellites météorologiques: MetOp, MeteoSat 2ème génération


Le CNES (Centre National d&#39;Etudes Spatiales) met ainsi à disposition de Copernicus plusieurs satellites comme Spot, Jason et Pleiades. D&#39;autres capteurs non spatiaux, dits in-situ, sont aussi mis à sa disposition par les États (avec une coordination au niveau européen), tels que des instruments aéroportés, ballons, bouées, stations de mesure, sismographes…

Par ailleurs, Copernicus compte sur les satellites Sentinels, spécifiquement développés dans le cadre du programme.

La série de satellites Sentinel est dédiée à l&#39;observation de la Terre, des océans et de l&#39;atmosphère. Les missions Sentinel ont les objectifs suivants :

- **Sentinel-1** : fourniture d&#39;imagerie radar tout-temps, jour et nuit, à des fins d&#39;observation du sol et des océans. Sentinel-1A a été lancé en 2014 et Sentinel-1B en 2016 pour atteindre l&#39;objectif d&#39;une fréquence de prise de vues de 6 jours pour l&#39;ensemble de la planète (résolution 9-40m).
 Le volume de données produites est de l&#39;ordre de 4-7 PB/année.

- **Sentinel-2** : fourniture d&#39;imagerie optique haute résolution pour l&#39;observation des sols (utilisation des sols, végétation, zones côtières, fleuves, etc.). Sentinel-2 est également utile pour la mise en place de services de traitement de l&#39;urgence. Le premier satellite Sentinel-2A a été lancé en 2015, et le satellite Sentinel-2B a été lancé en 2017 pour atteindre l&#39;objectif d&#39;une fréquence de prise de vues de 5 jours pour l&#39;ensemble de la planète (résolution 10-60m - 13 bandes spectrales).
 Le volume de données produites est de l&#39;ordre de 3-4 Petabytes/année.

- **Sentinel-3** : surveillance mondiale des océans et des sols. Deux satellites Sentinel-3 ont été lancés en 2016 et 2018, pour une fréquence de prise de vues inférieure à 2 jours (résolution 300-1200m).

Le volume de données produites est de l&#39;ordre de 3-4 Petabytes/année.

- **Sentinel-4** : observation de la composition chimique de l&#39;atmosphère à haute résolution temporelle et spatiale. Le satellite sera lancé en 2023.

- **Sentinel-5** : observation de la composition chimique de l&#39;atmosphère. Le satellite Sentinel-5p a été lancé en 2017 comme solution d&#39;attente au satellite Sentinel-5, plus complet, qui sera lancé en 2021. Il a pour objectif de mesurer la quantité de certains gaz et aérosols présents dans l&#39;atmosphère terrestre (quantité et répartition) ayant un impact sur la qualité de l&#39;air et le climat.
 Le volume de données produites par Sentinel-5p est de l&#39;ordre de 0.5 Petabytes/année.

- **Sentinel-6** : étude quasi temps réel de la surface des océans (vagues, vents de surface, hauteur de la mer)à destination des prévisions de météorologie marine à court terme, des prévisions saisonnières (El Niño, ...) et des recherches sur le changement climatique. Le premier satellite Sentinel 6A a été lancé en novembre 2021, et Sentinel 6B sera lancé en 2025.

#### Core Services

Les observations produites par l&#39;ensemble des satellites et capteurs à disposition du programme Copernicus sont diffusées sous la forme de services (&quot;Core Services&quot;), regroupant les informations en 6 thématiques.

Thématiques par milieu :

- Atmosphère ([CAMS](https://www.copernicus.eu/fr/services/atmosphere))
- Marin ([CMEMS](https://www.copernicus.eu/fr/services/marin))
- Terrestre ([CLMS](https://www.copernicus.eu/fr/services/terrestre))

Thématiques par thème:

- Changement climatique ([C3S](https://www.copernicus.eu/fr/services/changement-climatique))
- [Sécurité](https://www.copernicus.eu/fr/services/securite)
- Urgences ([Copernicus EMS](https://www.copernicus.eu/fr/services/urgence))

L&#39;objectif de chaque _Core Service_ est de fournir de la donnée à valeur ajoutée, à partir de l&#39;analyse et le traitement des données brutes issues de capteurs.

Des données s&#39;étalant sur des années, voire des dizaines d&#39;années, sont ainsi rendues consultables et comparables, facilitant la détection de tendances, motifs, et anomalies, le calcul de statistiques…

### ​4.2.​ Le projet DIAS (Data and Information Access Services)

Le projet Copernicus DIAS (Data and Information Access Services) est une initiative de la Commission Européenne, pilotée par l&#39;Agence Spatiale Européenne (ESA), dont l&#39;objectif est de faciliter l&#39;accès aux données du programme Copernicus, ainsi qu&#39;à des solutions de calcul dans le cloud pour exploiter ces données. In fine, la Commission Européenne souhaite ainsi ouvrir l&#39;usage des données du programme Copernicus à un panel nouveau d&#39;entreprises ou organismes, de façon à créer de nouveaux marchés ou dynamiser des domaines existants.

Pour cela, la stratégie de la Commission Européenne est de développer un écosystème de services (accès, calcul, stockage...) autour du programme Copernicus, de façon à attirer les utilisateurs via une offre large et compétitive.

Un appel d&#39;offre est lancé pour sélectionner plusieurs consortiums à même de mettre au point une plateforme de stockage et diffusion des données du programme Copernicus, ainsi qu&#39;une offre permettant à de futurs utilisateurs de réaliser des calculs sans avoir à investir dans des solutions lourdes.

D&#39;autre part, les consortiums s&#39;engagent à donner un accès illimité, gratuit et total aux données du programme Copernicus (Core Services compris) à tout type d&#39;utilisateurs.

Selon l&#39;ESA, le succès des DIAS va être fonction de l&#39;attractivité de leurs services et de leur capacité à proposer des solutions offrant en standard plus de qualité / valeur que celles qui pourraient être développées en interne par leur client. D&#39;autre part, le succès va dépendre de la capacité de chaque DIAS à attirer une communauté de fournisseurs de services basés sur chaque plateforme, de façon à créer un écosystème de services.

Cinq consortiums sont retenus en décembre 2017 pour mettre en oeuvre, de façon concurrente, le cahier des charges :

- **[CREODIAS](https://creodias.eu/)** : consortium représenté principalement par CloudFerro (Cloud Provider polonais) et Sinergise (société slovène spécialisée dans les solutions logicielles s&#39;appuyant sur de l&#39;imagerie spatiale)

- **[MUNDI WEB SERVICES](https://mundiwebservices.com/)** : consortium représenté principalement par Atos, T-Systems (Open Telekom Cloud), Thales Alenia Space et Sinergise (société slovène spécialisée dans les solutions logicielles s&#39;appuyant sur de l&#39;imagerie spatiale)

- **[ONDA](https://www.onda-dias.eu)** : consortium représenté principalement par Serco Italia, OVH, Gael Systems et Sinergise (société slovène spécialisée dans les solutions logicielles s&#39;appuyant sur de l&#39;imagerie spatiale)

- **[SOBLOO](https://sobloo.eu/)** : consortium représenté principalement par Airbus, Capgemini et Orange Business Services

- **[WEKEO](https://www.wekeo.eu/)** : consortium représenté principalement par Mercator Océan, EUMETSAT (organisation européenne pour l&#39;exploitation de satellites météorologiques) et ECMWF (centre européen pour les prévisions météorologiques à moyen terme)

Les cinq consortiums délivrent la première version de leur solution aux trimestres Q1/Q2 de 2018, et sont engagés contractuellement avec l&#39;ESA jusqu&#39;en décembre 2020.

## ​5.​ Etude comparative

### ​5.1.​ Services d&#39;accès aux données

Au cœur du projet DIAS se trouve la volonté de diffuser largement les données Copernicus à travers des plateformes gérées par des entités privées, de façon à créer une concurrence et encourager chaque plateforme à proposer des services innovants.

#### **Accès aux données**

Deux modes d&#39;accès aux données sont proposés:

- **Accès par téléchargement, permettant de télécharger directement les produits Copernicus.**


Dans ce cas, les produits ne sont pas altérés par rapport à ce qui est disponible sur le portail SentinelHub : pas de changement de format ou projection, pas de rééchantillonnage, l&#39;ensemble des données reste parfois zippé (sans possibilité de ne télécharger qu&#39;un fichier au sein de l&#39;archive ZIP).

La possibilité de ne télécharger que certaines bandes spectrales d&#39;un produit, proposée par certaines plateformes, est un atout non négligeable pour réduire les temps de téléchargement de données, aujourd&#39;hui point crucial dans le domaine du traitement d&#39;images spatiales.

Les services de téléchargement sont, selon les plateformes, disponibles de manière externe (téléchargement HTTP accessible depuis n&#39;importe où), et parfois interne au cloud de chaque plateforme pour bénéficier de bien meilleures performances de téléchargement du fait de la bande passante entre machines du cloud.

L&#39;accès à ces services de téléchargement se fait via des mécanismes d&#39;authentification : clés d&#39;accès à incorporer dans les URLS, tokens passés dans un cookie ou dans les entêtes HTTP…

- **Accès par flux OGC (WMS, WMTS, WFS) pour télécharger les produits Copernicus sur une zone et dans un format spécifique.**

Dans ce cas, chaque service OGC propose différents formats et projections géographiques en sortie, il est possible de demander une zone (via boîte englobante) et avec une taille d&#39;image voulue ou niveau de zoom (rééchantillonnage). Selon les plateformes, il est aussi possible de télécharger soit un flux RGB (Rouge/Vert/Bleu), soit chaque bande spectrale individuellement, soit des flux issus d&#39;un calcul comme du NDVI.

L&#39;accès à ces flux se fait aujourd&#39;hui sans authentification, puisque l&#39;ESA requiert que l&#39;accès aux données soit gratuit (pas besoin de vérifier l&#39;appelant puisque pas de tarification associée).

#### **Catalogage**

L&#39;ensemble des plateformes doivent proposer un système de catalogage disponible gratuitement et sans compte utilisateur, que ce soit via une interface graphique user-friendly ou via une API conforme aux standards du marché. L&#39;objectif est que les utilisateurs puissent indifféremment utiliser les API de catalogage de chaque DIAS, en s&#39;appuyant sur des protocoles standards.

Les plateformes implémentent toutes, sauf [WEKEO](https://www.wekeo.eu/), le protocole OpenSearch pour les catalogues et ont, pour plusieurs d&#39;entre elles, implémenté des API de catalogage basées sur des protocoles maison.

Quelque soient leur protocoles, les catalogues proposent des mécanismes de filtrage tels que des filtres spatiaux, temporels ou par type de produits. Les API permettent ensuite de télécharger un plusieurs produits filtrés à l&#39;aide du catalogue. Pour le téléchargement, il est néanmoins nécessaire de posséder un compte authentifié.

#### **Jeux de données**

Les plateformes doivent proposer l&#39;ensemble des données du programme Copernicus et peuvent proposer des jeux de données additionnels, de façon payante ou non, issus d&#39;autres fournisseurs.

Il s&#39;agit souvent d&#39;un atout pour les plateformes pour se démarquer.

Certaines plateformes proposent ainsi des données spatiales SPOT, PLEIADES, MUSCATE, LANDSAT, voire la donnée FluxVision Orange dans le cas de [SOBLOO](https://sobloo.eu/) (données anonymisées issues de la localisation des téléphones portables et bases de clients Orange).

Quelques plateformes proposent aussi déjà des données calculées (NDVI, SWIR, NDWI, Bathymétrie) pour éviter à leurs clients de devoir réaliser ces calculs.

#### **Données à chaud et à froid**

Les plateformes sont contractuellement dans l&#39;obligation de proposer l&#39;ensemble des données du programme Copernicus, mais ont la possibilité de proposer des disponibilités différentes selon le type de données, leur date, leur localisation ou tout autre critère jugé pertinent par chaque DIAS.

En pratique, on distingue donc les données à chaud, disponibles immédiatement via les services d&#39;accès aux données de chaque DIAS, et les données à froid qu&#39;il est nécessaire de commander auprès de chaque plateforme. Un délai d&#39;obtention est alors requis pour pouvoir ensuite accéder à ces données à chaud.

[SOBLOO](https://sobloo.eu/) et [ONDA](https://www.onda-dias.eu) proposent des APIs permettant de demander une donnée à chaud. Pour les autres plateformes, il est nécessaire de contacter un des représentants.
 [ONDA](https://www.onda-dias.eu) propose par ailleurs le service MOST (Managed [ONDA](https://www.onda-dias.eu) STorage) pour conserver dans un espace de stockage les données récupérées de leur stockage à froid, afin d&#39;obtenir de bonnes performances d&#39;accès par la suite.

#### **Délai de disponibilité des données**

Les produits proposés par chaque DIAS sont tous issus de plusieurs serveurs nommés DIAS 1, 2 et 3, mis à disposition par l&#39;ESA pour les plateformes DIAS.

Chacun de ces serveurs propose une copie des données du portail [SciHub](https://scihub.copernicus.eu/) de l&#39;ESA.

A compter de la mise à disposition d&#39;un produit sur SciHub, le temps de réplication des données sur un des serveurs DIAS 1/2/3 est théoriquement de 6h.

Dans le tableau ci-dessous, les délais de mise à disposition de données sur une plateforme DIAS sont à considérer avec précaution, car issus des entretiens menés avec les représentants des DIAS et peut-être plus alléchants que réels. Ces temps restent néanmoins a priori tous courts, de l&#39;ordre de 1-2h.

#### **Synthèse des offres de fourniture de données proposées par chaque plateforme**

Ce tableau présente différents critères de comparaison entre plateformes DIAS pour la fourniture de données.

| | **[CREODIAS](https://creodias.eu/)** | **[MUNDI WEB SERVICES](https://mundiwebservices.com/)** | **[ONDA](https://www.onda-dias.eu)** | **[SOBLOO](https://sobloo.eu/)** | **[WEKEO](https://www.wekeo.eu/)** |
| --- | --- | --- | --- | --- | --- |
| **Produits disponibles** | Sentinel 1 GRD /OCN / RAW / SLC,  Sentinel 2 L1C / L2A, Sentinel 3 (tout), Sentinel 5p L1B / L2  Core Services, Landsat 5/7/8 SMOS, S2GL, Envisat MERIS, RapidEye, Planet Scope, DEM SRTM et Mapzen, Jason-3, Jilin-1, KazEOSat, KOMPSAT 2 / 3 / 3A / 5, Copernicus Climate Change Service (C3S), MODIS Terra / Aqua | Sentinel 1 GRD / OCN / L1C, Sentinel 2 L1C / L2A (L2A sur Europe seulement), Sentinel 3 SRAL / OLCI / SLSTR /SYNERGY, Sentinel 5p L1B / L2 Core Services, Landsat 8 (Europe seulement), Très haute résolution (Digital Globe) via partenaire GAF, Données météo Ubyrisk, Geoadventice | Sentinel 1 GRD /OCN / RAW / SLC, Sentinel 2 L1C / L2A, Sentinel 3 (tout), Sentinel 5p L1B / L2 Core Services, Envisat, Landsat 8, KOMPSAT, Deimos-2Maxar | Sentinel 1 GRD /OCN / RAW / SLCSentinel 2 L1C / L2A,Sentinel 3 (tout),Sentinel 5p L1B / L2, Core Services, Spot 6/7, Pleiades A/B, Muscate, soCAP, Flux Vision : Données GeoMarketing Orange | Sentinel 1 GRD /SLC,Sentinel 2 L1C,Sentinel 3 (tout),Sentinel 5p, Core Services |
| **Délai pour disponibilité d&#39;un produit sur la plateforme** | 1h à compter de leur mise à disposition sur les répliques DIAS 1/2/3 | 1h à compter de leur mise à disposition sur les répliques DIAS 1/2/3 | 1-2h à compter de leur mise à disposition sur les répliques DIAS 1/2/3 | 1-2h à compter de leur mise à disposition sur les répliques DIAS 1/2/3 | Non communiqué |
| **Volume disponible à chaud** | 30 Petabytes de stockage | 8 Petabytes de stockage en 2020 (volume 2022 non connu) | 844 Terabytes à chaud (25.6 Petabytes à froid) | 6-7 PetaBytes en 2020 (volume 2022 non connu) / Proposent une API de Data Warming, pour charger des données qui seront ensuite disponibles à chaud. Délai jusqu&#39;à 6 jours | 4.7 Petabytes en 2019. / Sentinel 1 et 2 seulement / Volume non communiqué en 2020 et 2022 |
| **Règles de rétention à chaud** | Politique de rétention assez complexe (décrite dans la section Data Offer de leur site) sur les produits, dates,localisation et détails de chaque produit. Privilégient l&#39;Europe, soit sans limite de rétention, soit pour les 6 derniers mois | 1 an de rétention à l&#39;échelle monde, et 2 ans sur l&#39;Europe (disent qu&#39;il s&#39;agit de l&#39;engagement contractuel des DIAS avec l&#39;ESA) | 1 mois véritablement à chaud, 10 minutes annoncées pour extraire n'importe quelle donnée à froid | 9 derniers mois pour produits Sentinel (et couverture nuageuse \&lt; 50% pour Sentinel 2). 3 mois pour Copernicus Core Service. Les produits SPOT et PLEIADES sont acquis sur demande. | Non communiqué |
| **Téléchargement HTTP externe** | EOFinder API | OpenSearch API | API OData (Open Data Protocol)OpenSearch API | OpenSearch APISearch API maisonDirect data API en beta | HDA API |
| **Téléchargement HTTP interne** | S3 et montage NFS (pas HTTP) | S3 | [ONDA](https://www.onda-dias.eu) Advanced API (ENS - Elastic Node Server) | Direct Data API (encore en beta) | Accès direct via service Object Storage et NFS dans le cloud |
| **Possibilité de ne télécharger que certaines bandes ?** | Non via APIOui via S3 | Oui via API et S3 | Non | oui avec Direct data API en beta | Non testé |
| **Systèmes de catalogage** | AP OpenSearch et API maison | API OpenSearch | AP OpenSearch et API maison | AP OpenSearch et API maison | OGC CSW et API custom (HDA) |
| **Produits accessibles en WMS/WMTS** | Sentinel 1/2/3, Landsat 5/7/8, Envisat, Meris, RapidEye, Planet Scope | Sentinel 1/2/3/5p a minima | Sentinel-1 GRD, Sentinel-2 L1C, Sentinel-2 L2A (seulement les 3 derniers mois) | S2 L1C and S1 GRD a minima. Vont proposer du WMTS à la demande | Disponible en 2021 (initialement prévu fin 2019) |
| **Contenu WMS/WMTS** | Accès direct aux bandes, ainsi qu&#39;à des bandes calculées (NDVI, SWIR, NDWI). Pas d&#39;accès direct à un produit a priori, seulement à une mosaïque de produits agrégés | Accès direct aux bandes, ainsi qu&#39;à des bandes calculées (NDVI, SWIR, NDWI, Bathymétrie, Geologie, indexes de végétation et humidité...).  Pas d&#39;accès direct à un produit, seulement à une mosaïque de produits agrégés | Mix RGB (&quot;TrueColor&quot;) seulement. Pas d&#39;accès direct à un produit a priori, seulement à une mosaïque de produits agrégés | Mix RGB seulement. Accès pour un seul produit à la fois (pas d&#39;agrégation de produits en une seule couche) | Aucun |
| **Formats WMS** | JPEG, PNG, TIFF, JPEG2000, RAW | JPEG, PNG, TIFF (8, 16, 32 bits) | JPEG, PNG, TIFF | JPEG, PNG, TIFF 24 bits | / |
| **Projections WMS** | De très nombreuses projections | De très nombreuses projections | EPSG:4326EPSG:4258EPSG:3857CRS:84 | EPSG:4326 seulement | / |
| **Taux de transfert externe** | 11 MB/sec (DataCenter en Pologne) | 14.7 MB/sec (DataCenter en Allemagne) | 5 MB/sec (Datacenter en France) | 22 MB/sec avec Direct Data API (DataCenter en France) | Non testé |
| **Taux de transfert interne** | 200 MB/sec | 200 MB/sec | 2000 MB/sec | 77 MB/sec (avec Direct Data API). Sûrement mieux en S3, mais non préconisé par [SOBLOO](https://sobloo.eu/) | Non testé |
| **Différenciants** | Proposent des produits précalculés : NDVI, SWIR, NDWI… Proposent des calculs à la volée via leur API OGC. Service payant de fourniture d&#39;imagerie Sentinel-1 GRD sous la forme de COG (Cloud Optimized GeoTiff) pour données \&lt; 30 jours | Données Landsat 8. Flux WMS avec indicateurs NDVI, SWIR, NDWI, Bathymétrie, Geologie, indexes de végétation et humidité... Service payant de fourniture d&#39;imagerie sous la forme de COG (Cloud Optimized GeoTiff) pour données \&lt; 3 mois | Données Landsat 8. Service [ONDA](https://www.onda-dias.eu) Data Relay pour s&#39;abonner à un services de fournitures d&#39;imagerie à la demande, sur une zone et selon une fréquence. Fourniture de "Analysis Ready Data", autrement dit des produits précalculés. | Imagerie SPOT, Pleiades, Muscate, soCAP et données FluxVision. API de Data Warming | [WEKEO](https://www.wekeo.eu/) se destine essentiellement à l&#39;éducation et la recherche et a bénéficié d&#39;un cahier des charges moins strict par rapport à celui des autres plateformes. Il est difficile de comparer leur offre (pas de pricing défini) à celle des autres plateformes. |

### ​5.2.​ Services de calcul et offre cloud

Au-delà d&#39;un accès simplifié aux données, répondant autant que possible à des protocoles standards, l&#39;objectif de l&#39;ESA avec le projet DIAS est de répondre à la difficulté rencontrée par les usagers de données spatiales pour les exploiter.
 En effet, les volumes de données à manipuler et les systèmes à mettre en place pour réaliser des calculs sur ces données peuvent être un frein à l&#39;utilisation des données Copernicus.

Pour répondre à cet objectif, le cahier des charges du projet requiert la mise en place par chaque consortium d&#39;une offre proposant à ses clients :

- un _environnement de calcul_ très performant (CPU, RAM , stockage, bande passante, accès performant aux données du programme Copernicus stockées sur chaque plateforme). Cet environnement de calcul/stockage doit pouvoir être monitoré par les utilisateurs, notamment en termes de consommation de ressources et de tarification.

- des _outils de calcul_ autant que possible basés sur l&#39;Open Source, a minima SNAP (suite logicielle de l&#39;ESA permettant la manipulation d&#39;imagerie satellite) et DHUS (solution web de l&#39;ESA pour le catalogage et téléchargement de données).

La réponse apportée par les cinq plateformes a sensiblement été la même, à savoir proposer une offre cloud en s&#39;appuyant sur un partenaire spécialisé dans le domaine : Orange Cloud, OVH, Open Telekom Cloud, CloudFerro…

Ces partenaires ont par ailleurs pour rôle d&#39;héberger les applications et données de chaque plateforme.

#### **Offres cloud**

Les offres cloud proposées par chaque plateforme sont stricto sensu celles de leur partenaires _cloud providers_, sans frais additionnels semble-t-il.

Ces offres s&#39;appuient toutes sur la suite OpenStack (solution logicielle permettant de concevoir des clouds publics et privés), complétée de surcouches fournies par chaque cloud provider.

Les deux principaux types de services cloud sont :

- _Instances de calcul_ : Machines virtuelles basées sur des images (linux, windows et éventuellement logiciels pré-installés), pilotables via une API

- _Object Storage_ : Solution de stockage (lecture/écriture) de données sans limite de taille et basée sur le protocole HTTP (pour pouvoir accéder aux données depuis un navigateur web).
 Les temps d&#39;accès en lecture/écriture restent néanmoins très inférieurs à ceux disponibles sur un _filesystem_ classique d&#39;une machine physique, du fait des nombreuses couches d&#39;abstraction nécessaires au fonctionnement d&#39;un tel système.

Chaque plateforme propose donc avec ces deux solutions le moyen pour un client d&#39;installer ses applications (sur les instances de calcul) et d&#39;y stocker des données (par exemple, les résultats de ses calculs sur Object Storage).

Chaque provider cloud propose par ailleurs de nombreux services (High Performance Computing, Load Balancing, Cache, Elasticité, Lambda Functions, MapReduce, Backup…) et options qu&#39;il n&#39;est pas possible de comparer dans le cadre de l&#39;étude, mais qui peuvent être déterminant pour les performances et la facilité de monitoring/déploiement de certaines applications. Il est recommandé de faire appel à un architecte cloud, ayant une expérience en matière de calcul hautes performances sur de l&#39;imagerie, pour réaliser des choix pertinents pour chaque application.

#### **Outils proposés**

Chaque plateforme propose des _images de VM_ bénéficiant d&#39;un certain nombre d&#39;outils de développement et calcul pré-installés.

L&#39;intérêt est de permettre aux clients d&#39;économiser le temps d&#39;installation de ces outils, avec les éventuelles difficultés d&#39;installation afférentes.

Néanmoins, les outils proposés (décrits dans le tableau ci-dessous) restent assez standards et aisés à installer, et ne sont pas de forts différenciants par rapport à l&#39;offre proposée par d&#39;autres cloud providers.

D&#39;autre part, il est rare qu&#39;une architecture de calcul ait besoin d&#39;avoir tous ces outils installés sur chaque serveur, et les professionnels de l&#39;infrastructure serveur tendent au contraire à privilégier des configurations aussi minimales que possible, incorporant seulement les outils/bibliothèques indispensables aux applications.

Ces configurations standards, bardées d&#39;outils pré-installés, ont donc un intérêt surtout comme bac à sable, facile à prendre en main pour réaliser des tests et prototypes.

Dans ce contexte, un des outils particulièrement intéressant (proposé par chacune des plateformes) est _Jupyter Notebook_, application web Python permettant de programmer dans divers langages (Python, R, Scala…) des &quot;notebooks&quot;. Ces notebooks sont des &quot;cahiers électroniques&quot; qui, dans le même document, peuvent rassembler du texte, des images, des formules mathématiques et du code informatique exécutable. Ils sont manipulables interactivement dans un navigateur web, et sont très prisés des Data Scientists pour concevoir des prototypes.

Une des plateformes ([MUNDI WEB SERVICES](https://mundiwebservices.com/)) se distingue néanmoins en proposant des services de calcul (WPS et pipeline), managés par la plateforme, et permettant à leurs clients de déployer leurs algorithmes pour exécution sur les nouveaux jeux de données dès qu&#39;ils sont disponibles. Il s&#39;agit d&#39;un service réellement différenciant et à forte valeur ajoutée.

#### **Bureau à distance**

L&#39;ensemble des plateformes propose, même si cette information est parfois mal documentée ou peu claire, un accès distant de type RDP (Remote Desktop Protocol) ou VNC (Virtual Network Computing) permettant de se connecter au bureau virtuel des Machines Virtuelles.

Cet accès peut être intéressant pour lancer des applications graphiques, comme QGIS, et économiser à la fois le temps d&#39;installation de ces logiciels sur son propre poste ou économiser le temps de téléchargement (et espace de stockage) des données visualisées.

Les systèmes de bureau à distance ont néanmoins pour défaut d&#39;être dépendant de la latence réseau entre le poste de travail et le serveur, ce qui peut parfois rendre les interactions avec les applications plus lentes et moins confortables que sur un poste local.

#### **Accès performant aux données**

Le principal intérêt de l&#39;offre cloud proposée par chaque plateforme est de permettre l&#39;installation des applications de calcul au plus près de là où sont stockées les données, dans le même datacenter.

En effet, les bandes passantes disponibles à l&#39;intérieur d&#39;un Data Center sont extrêmement élevées et il est possible pour chaque DIAS de proposer des API internes, disponibles depuis les machines virtuelles d&#39;un client, avec des temps d&#39;accès aux données très faibles.

#### **Services d&#39;ingénierie**

L&#39;offre de chaque DIAS comprend par ailleurs systématiquement un service payant d&#39;aide à la conception d&#39;applications et architectures de calcul répondant aux besoins de chaque client.

Ce service est disponible sur demande, et devis, auprès des représentants de chaque DIAS.

#### **Marketplaces**

Dernier élément de l&#39;offre de calcul proposée par chaque plateforme, une marketplace permet à chaque client de présenter sa ou ses solutions sur le portail de chaque plateforme.

En effet, l&#39;enjeu pour les plateformes est d&#39;assurer leur réussite commerciale, et elles ont tout intérêt à promouvoir les solutions qui s&#39;appuient sur leur offre pour attirer une clientèle nouvelle.

Pour les clients, c&#39;est en retour la garantie d&#39;être aidé par chaque DIAS à se faire connaître, que ce soit via la Marketplace ou plus généralement via le discours commercial de chaque plateforme pour se mettre en valeur grâce à sa clientèle.

#### **Synthèse des offres de calcul proposées par chaque plateforme**

| | **[CREODIAS](https://creodias.eu/)** | **[MUNDI WEB SERVICES](https://mundiwebservices.com/)** | **[ONDA](https://www.onda-dias.eu)** | **[SOBLOO](https://sobloo.eu/)** | **[WEKEO](https://www.wekeo.eu/)** |
| --- | --- | --- | --- | --- | --- |
| **Outils de calcul** | SNAP, GDAL, Orfeo Toolbox, MapServer/GeoServer, QGISArcGIS | SNAP, GDAL, Orfeo Toolbox, MapServer/GeoServer, QGIS, Services OGC WPS et pipeline de calcul | SNAP, GDAL, Orfeo Toolbox, QGIS | SNAP, BRAT, QGIS, GDAL, Orfeo Toolbox, Sen2Core, SNAPHU | SNAP, Orfeo Toolbox, QGIS, Grass, ArcGIS Imagery Workflow, Numpy, Scipy, Pandas, outils de traitement image et solutions SIG... |
| **Autres outils/solutions** | Outils de développement comme Python... | Non documenté | Outils de développement comme GCC, Maven, Git, R… Solution Data Hosting Services pour servir ses propres données image Service [ONDA](https://www.onda-dias.eu) Data Relay pour s&#39;abonner à un services de fournitures d&#39;imagerie à la demande, sur une zone et selon une fréquence | SDK [SOBLOO](https://sobloo.eu/) (Python), basé sur la bibliothèque eodag, pour faciliter l&#39;utilisation des services de [SOBLOO](https://sobloo.eu/). Outils de développement comme GCC, Git, Python, R… Solution pour servir ses propres données image | Outils de développement comme Python, R |
| **Remote desktop** | Oui | Oui | Oui | Oui | Oui a priori |
| **Jupyter Notebook** | Oui | Oui | Oui | Oui | Oui |
| **Différenciants** | ArcGIS | Services OGC WPS et pipeline de calcul | Solution Data Hosting Services pour servir ses propres données image. Service [ONDA](https://www.onda-dias.eu) Data Relay pour s&#39;abonner à un services de fournitures d&#39;imagerie à la demande, sur une zone et selon une fréquence | Cloud Orange implanté sur les cinq continents (70 datacenters). Solution pour servir ses propres données image | |

#### **Comparatif des temps de chargement de données**

Nous avons incorporé à l&#39;étude une mesure des temps de téléchargement depuis chaque plateforme, de façon externe (autrement depuis nos postes de travail) et de façon interne, c&#39;est-à-dire depuis une VM hébergée dans le datacenter de chaque DIAS et mise en place dans le cadre d&#39;une offre d&#39;essai.

Ces temps sont basés sur le téléchargement du même jeu de données. Nous avons répété l&#39;opération à plusieurs moments pour obtenir une moyenne.

Les temps externes sont à relativiser, car ils dépendent de la bande passante de nos postes de travail (fibre optique) et de leur distance au datacenter de chaque DIAS.

Dans chaque cas, nous avons utilisé une ou plusieurs API proposées par chaque DIAS, avec une préférence pour les API internes quand celles-ci sont disponibles.

<br/>

**Taux de téléchargement externes**

| | Taux moyen de téléchargement (Mb/s) |
| --- | --- |
| [SOBLOO](https://sobloo.eu/) API (externe) | 6,51 |
| [SOBLOO](https://sobloo.eu/) Direct data API (externe) | 8,52 |
| [CREODIAS](https://creodias.eu/) API (externe) | 5,20 |
| [ONDA](https://www.onda-dias.eu) API (externe) | 6,39 |
| [MUNDI WEB SERVICES](https://mundiwebservices.com/) API (externe) | 6,39 |
| [WEKEO](https://www.wekeo.eu/) API (externe) | 5,59 |
<br/>

**Taux de téléchargement internes aux VMs**

| | Taux moyen de téléchargement (Mb/s) |
| --- | --- |
| [SOBLOO](https://sobloo.eu/) API (interne VM) | 6,57 |
| [SOBLOO](https://sobloo.eu/) Direct data API (interne VM) | 165,23 |
| [CREODIAS](https://creodias.eu/) API (interne VM) | 63,01 |
| [CREODIAS](https://creodias.eu/) (S3 interne VM) | 117,51 |
| [ONDA](https://www.onda-dias.eu) API (interne VM) | 20,24 |
| [ONDA](https://www.onda-dias.eu) (NFS interne VM) | 211,00 |
| [MUNDI WEB SERVICES](https://mundiwebservices.com/) API (interne VM) | 188,08 |
| [MUNDI WEB SERVICES](https://mundiwebservices.com/) (S3 interne VM) | 201,58 |
| [WEKEO](https://www.wekeo.eu/) API (interne VM) | 19,79 |
<br/>

### ​5.3.​ Tarification

Un des aspects les plus complexes à aborder, et à comparer, avec les plateformes DIAS est la tarification proposée.

L&#39;ESA a fait le même constat et a demandé début 2020 aux plateformes de simplifier leur proposition tarifaire. Des améliorations en la matière devraient arriver en 2021.

#### **Un dénominateur commun très difficile à comparer**

A ce jour, l&#39;accès aux données du programme Copernicus reste gratuit pour toutes les plateformes, quel que soit le mécanisme d&#39;accès utilisé (téléchargement interne ou externe, flux OGC).

Les jeux de données additionnels, hors programme Copernicus, sont payants, sans tarification publique. Les prix habituellement proposés pour ces jeux de données sont à prévoir.

[SOBLOO](https://sobloo.eu/) précise néanmoins que sa tarification pour les données SPOT se base sur un volume de données inférieur à celui sur lequel s&#39;appuie la tarification pratiquée par Airbus Defense and Space. Autrement dit, pour des utilisateurs ayant besoin de faibles volumes de données, ou travaillant sur de petites zones, la tarification proposée par [SOBLOO](https://sobloo.eu/) sera moins coûteuse.

Quant aux services de calcul, le seul réel dénominateur commun entre les plateformes est leur offre cloud. En la matière, la comparaison est aussi extrêmement complexe car de l&#39;ordre de la comparaison de complémentaires santé…

Chaque cloud propose des services et options pratiquement impossibles à comparer avec le cloud voisin, avec des configurations de VM qui ne sont jamais alignées avec la concurrence et des mesures de performances valables pour des cas d&#39;usage tellement précis qu&#39;il serait trompeur de les généraliser.

#### **Solution pour comparer**

Pour comparer la tarification de deux plateformes, il convient donc en premier lieu de spécifier assez précisément le cas d&#39;usage que l&#39;on souhaite développer puis d&#39;étudier comment le résoudre avec chaque plateforme, en fonction de ce que chacune propose.

Idéalement, réaliser un POC avec chaque plateforme peut aider à se rendre compte des performances obtenues, et éventuelles contraintes techniques nécessitant l&#39;usage de plus de services cloud que prévu sur le papier…

Une comparaison des tarifs devient alors possible, en intégrant tous les coûts, mais cela reste beaucoup de travail pour y arriver.

### ​5.4.​ Etude de quelques cas d'usage

**Contexte**

Dans le cadre de la mise à jour de l'étude réalisée en 2022, financée par le CNES dans le cadre de son projet DIAS Priming, il a été convenu de combler quelques lacunes de l'étude en intégrant divers cas d'usage permettant de comparer les plateformes DIAS.

Ces cas d'usage ont été extraits par SOMEWARE d'une analyse de besoins de quatre entreprises, sélectionnées par le CNES dans le cadre de l'accompagnement DIAS Priming, souhaitant implanter leurs chaînes de traitements sur les DIAS.

A partir des besoins propres à chaque entreprise, Someware a cherché à identifier les facteurs clé déterminant le choix d’une plateforme en fonction d’un cas d’usage : nature des données nécessaires, performances de téléchargement / calcul attendues, caractéristiques de l’environnement cloud….
Ensuite, Someware a réalisé une étude comparative des plateformes DIAS au regard de ces facteurs, en cherchant à obtenir auprès de chaque plateforme les informations nécessaires à l’identification de la ou des plateformes les plus adaptée(s) à chaque cas d’usage.

Les cas d'usage des 4 sociétés sont décrits ci-dessous et anonymisés autant que possible.

#### **Société A / Traitement à très grande échelle**

Contexte/besoins :
```
La société A met au point des produits satellitaires à échelle mondiale, et se spécialise sur le traitement des couleurs.
Elle développe une chaîne de traitement combinant les outils Maja/Wasp ainsi que Sen2core pour produire un niveau L3A mondial.
Les difficultés qu'elle rencontre aujourd'hui sont liés aux temps de téléchargement / traitement longs des données pour une telle échelle de traitement.
D'autre part, elle ne maîtrise pas le déploiement de calculs dans le cloud.
```

Solutions possibles avec les DIAS:
```
Concernant l'accès à des onnées sur n’importe quelle zone avec forte profondeur temporelle :

- Creodias propose l’ensemble des données Sentinel-2 L1C en téléchargement, et ne prévoit pas d’arrêt du service

- Les API de Mundi, Onda et Sobloo permettant d’obtenir en téléchargement des données de l’ensemble du catalogue Copernicus semblent aussi particulièrement adaptées. Onda annonce un délai de disponibilité de 10 minutes pour les données demandées, avec un temps de rétention à chaud de 24 heures.

Concernant l'optimisation des temps de calcul / téléchargement :

- Les DIAS n’offrent rien de particulier pour optimiser les temps de calcul. Leur offre cloud est sensiblement la même que n’importe quel cloud.

- Le téléchargement peut néanmoins être optimisé en étant déployé chez la plupart des DIAS (voir étude) grâce aux accès direct aux espaces “Object Storage” hébergeant les données.
```

#### **Société B / Accès aux données de (et depuis) la zone Pacifique Sud**

Contexte/besoins :
```
La société B met au point des outils d’aide à la décision basés sur la télédétection à destination des territoires insulaires du Pacifique Sud (où la société est basée).
Elle a pour projet de mettre au point et diffuser des données L2A (outils Maja / Sen2core) à l’échelle du Pacifique Sud.
D'autre part, elle souhaite accéder aux données Sentinel-1 dès leur mise en ligne pour un projet de surveillance maritime.
De façon générale, son besoin est de pouvoir accéder à des produits bruts récents ou données traitées à l’échelle du Pacifique Sud, avec de bons temps de téléchargement.
Elle dispose en revance de compétences en déploiement de calculs dans un data center local.
```

Solutions possibles avec les DIAS:
```
Concernant l'accès à des données sur la zone Pacifique Sud :

- Creodias propose le plus gros catalogue de données à chaud (notamment l’ensemble des données Sentinel-2 L1C), et ne prévoit pas d’arrêt du service. Voir https://creodias.eu/data-offer

- Les API de Mundi, Onda et Sobloo permettant d’obtenir en téléchargement des données de l’ensemble du catalogue Copernicus semblent aussi particulièrement adaptées.

- Onda annonce un délai de disponibilité de 10 minutes pour les données demandées, avec un temps de rétention à chaud de 24 heures.

- Par ailleurs, Mundi peut proposer une offre de calcul de données L2A à des prix a priori compétitifs, dès la parution d’une image notamment

Concernant le temps de téléchargement depuis la zone Pacifique Sud :

- L’ensemble des DIAS s’appuient sur des data centers en Europe. Seul Sobloo proposait des data centers Orange à échelle mondiale (notamment en Australie a priori), mais le partenariat Airbus / Orange semble s’arrêter.

- Il est sans doute possible de gagner en performance en faisant transiter les données d’Europe vers un datacenter local branché sur une dorsale (backbone) internet. L’automatisation / parallélisation massive des téléchargements doit aussi être envisagée.
```

#### **Société C / Optimisation de temps de téléchargement / traitement**

Contexte/besoins :
```
La société C met au point un service d’analyse géographique, essentiellement en Afrique, à partir d’imagerie Sentinel-1 et Sentinel-2.
Elle maîtrise l'automatisation ainsi que la conception d'algorithmes de traitement, mais pas le déploiement de calculs dans le cloud.
Elle souhaite globalement optimiser ses temps de téléchargement / traitement et, si possible, pouvoir déclencher ses traitements dès qu'une donnée est disponible (notion de "triggers").
Elle cherche aussi à accéder à des données sur de vastes zones, avec une forte profondeur temporelle.
```

Solutions possibles avec les DIAS:
```
Concernant la disponibilité de données sur de vastes zones / forte profondeur temporelle:

- Creodias propose l’ensemble des données Sentinel-2 L1C en téléchargement, et ne prévoit pas d’arrêt du service
- Les API de Mundi, Onda et Sobloo permettant d’obtenir en téléchargement des données de l’ensemble du catalogue Copernicus semblent aussi particulièrement adaptées.
- Onda annonce un délai de disponibilité de 10 minutes pour les données demandées, avec un temps de rétention à chaud de 24 heures.

Concernant la possibilité de déclencher des calculs dès qu'un donnée est disponible :

- Mundi propose un service de déclenchement de calcul dès qu’une donnée est publiée, avec publication des données résultantes via les webservices de la plateforme

- Onda recommande du “polling” pour requêter régulièrement son catalogue

- Ce mécanisme est enfin étudié par Airbus dans le cadre de sa future plateforme ExtenSO (successeur a priori de Sobloo)

```

#### **Société D / Chargement de données pour une interface de WebMapping**

Contexte/besoins :
```
La société D développe des solutions de cartographie multi-source, basée notamment sur des données spatiales.
Elle exploite notamment des données Sentinel-1, Sentinel-2 et Pléiade Neo en Europe et Afrique.
Son usage le plus courant est le chargement de données en fonds de plan (tuiles / WMTS) dans une interface de WebMapping.
```

Solutions possibles avec les DIAS:
```
Concernant l'accès à des données au sein de solutions de WebMapping :

 - L’ensemble des DIAS proposent des données Sentinel-2 couleur sous la forme de flux WMS (besoin d’installer un serveur WMTS devant), voire WMTS pour Mundi.
 
Concernant le téléchargement de données sur l'Afrique :

 - L’Europe est la zone privilégiée par les plateformes DIAS. Les zones hors-Europe sont généralement peu couvertes ou alors avec une très faible profondeur temporelle.

- Néanmoins certaines plateformes (Mundi, ONDA ou Sobloo par exemple) permettent d’obtenir de façon automatique et rapide (API) des données de l’ensemble du catalogue Copernicus en téléchargement. CreoDIAS propose par ailleurs le plus gros catalogue de données à chaud.

Enfin, les données Pléiades Neo sont encore trop récentes, avec une faible couverture spatiale. Elles ne sont pas à ce jour proposées par les DIAS.
```

### ​5.5.​ Analyse critique

L&#39;étude comparative ne saurait être complète sans apporter un éclairage sur divers critères, non fonctionnels, qui peuvent être décisifs dans l&#39;adoption (ou le rejet) d&#39;une plateforme.

Les critères suivants, assez subjectifs, ont été identifiés comme particulièrement déterminants :

- _Facilité d&#39;échange avec les représentants_ : bien que les portails permettent de d&#39;inscrire de façon autonome, la relation avec une plateforme commence généralement par un échange avec ses représentants, cherchant à comprendre les besoins et le type de client auquel ils ont affaire. Selon les plateformes, nous avons noté des délais de réponse très variables, bien loin parfois des délais annoncés sur les plaquettes commerciales (Support 7J/7 24h/24), et des réponses techniques parfois laborieuses.

- _Qualité de la documentation_ : indispensable pour travailler en autonomie. Nous avons beaucoup utilisé les documentations pour cette étude et avons rencontré des difficultés variées pour trouver les informations nous permettant de réaliser certaines opérations.

- _Interface_ : avant d&#39;utiliser les API (catalogage, OGC, téléchargement…), nous avons cherché à utiliser les interfaces web proposées par chaque plateforme, de façon à visualiser les produits disponibles, réaliser des recherches dans le catalogue et télécharger des produits. Ces interfaces, certainement peu utiles pour des applications ou utilisateurs rodés, sont néanmoins un point d&#39;entrée indispensables pour s&#39;approprier une plateforme.

- APIs : nous avons évalué la facilité de prise en main des APIs d&#39;accès aux données et de catalogage, ainsi que leur richesse fonctionnelle

Pour donner une vision synthétique de l&#39;analyse critique de chaque plateforme, les parties ci-après listent aussi ces critères plus objectifs, déjà mentionnés dans les tableaux comparatifs précédents :

- _Volume de données proposées à chaud_ : ces données sont immédiatement disponibles en téléchargement, sans délai de mise à disposition

- _Types de données_ : il s&#39;agit des données proposées, incluant les données Copernicus mais aussi d&#39;autres jeux de données (généralement payants) pouvant différencier une plateforme

- _Disponibilité de données/indicateurs calculés_ : données ou indicateurs calculés par chaque plateforme, qu&#39;il n&#39;est donc pas nécessaire de recalculer soi-même. Nous avons estimé que proposer cette fonctionnalité offrait une plus-value aux plateformes.

- _Fonctionnalités_ : au-delà des fonctionnalités minimum requises (catalogage, téléchargement, services OGC), il s&#39;agit des fonctionnalités proposées par chaque DIAS pour différencier leur offre. Nous avons choisi de privilégier, dans notre notation, les fonctionnalités dédiées au calcul, car l&#39;offre des plateformes DIAS est de manière générale pauvre en la matière.

- _Performances_ : il s&#39;agit des performances de téléchargement de données, externes (accès depuis son poste) et internes (accès depuis une VM du cloud). Nous avons privilégié la performance interne dans notre notation, car c&#39;est le point fort des DIAS, plus que l&#39;accès aux données en externe.

Nous n&#39;avons pas cherché à mesurer les performances de calcul des VMs car ce sont des métriques très difficiles à estimer, dépendant de trop nombreux facteurs (CPU, RAM, I/O disque, réseau…).

#### **[CREODIAS](https://creodias.eu/)**

|     |     |     |
| --- | --- | --- |
| **Documentation** ⬤⬤⬤⬤◯<br/> Documentation bien écrite, complète, avec des exemples de code, mais mal organisée. Il est néanmoins possible d&#39;être autonome en tant que développeur | **Echanges** ⬤⬤◯◯◯<br/> Réactivité moins bonne que celle d&#39;autres plateformes. Échanges techniques parfois laborieux. Les interlocuteurs en charge du support sont des spécialistes du cloud (employés de CloudFerro) qui connaissent mal la plateforme [CREODIAS](https://creodias.eu/) et renvoient vers la documentation. | **Performances** ⬤⬤⬤◯◯<br/> Performances internes pour l&#39;accès aux données parmi les plus basses. Performances externes correctes, sans doute pénalisées par la localisation du datacenter en Pologne. |
| **Volume à chaud** ⬤⬤⬤⬤⬤<br/> Le plus grand volume disponible à chaud (30 PB) | **Types de données** ⬤⬤⬤⬤◯<br/> Copernicus + Landsat 5/7/8 + Envisat + SMOS + SRTM + Jason-3 + Jilin-1 + KazEOSat + KOMPSAT | **Données calculées** ⬤⬤⬤⬤◯<br/> Quelques jeux de données calculés : NDVI, NDWI, SWIR |
| **Interface** ⬤⬤⬤⬤⬤<br/> Deux interfaces: une pour télécharger des produits &quot;bruts&quot;, intuitive, et l&#39;autre pour télécharger des données calculées, moins simple d&#39;utilisation | **APIs** ⬤⬤⬤◯◯<br/> API de téléchargement et catalogage faciles d&#39;utilisation. L&#39;API OGC proposée est celle de SentinelHub, pas développée spécifiquement pour [CREODIAS](https://creodias.eu/) | **Fonctionnalités** ⬤⬤⬤⬤◯<br/> Service payant de fourniture d&#39;imagerie Sentinel-1 GRD sous la forme de COG (Cloud Optimized GeoTiff) pour données \&lt; 30 jours. Services OGC avec calcul à la volée. ArcGIS proposé sur les VM |

**En résumé :**

Quelques très bons points (volume, données calculées…), mais un support peu efficace piloté par des experts du cloud connaissant mal la plateforme [CREODIAS](https://creodias.eu/).

Les performances internes de téléchargement des données sont un peu décevantes.

[CREODIAS](https://creodias.eu/) s&#39;appuie majoritairement sur les compétences du cloud provider CloudFerro, qui opère aussi la plateforme [WEKEO](https://www.wekeo.eu/) et mutualise des ressources entre [CREODIAS](https://creodias.eu/) et [WEKEO](https://www.wekeo.eu/).

[CREODIAS](https://creodias.eu/) revendique environ 3000 utilisateurs, principalement issus du monde de la recherche. Les projets menés par ces utilisateurs ont une durée d&#39;un an en moyenne.

CLOUDFERRO déclare en 2022 vouloir poursuivre la maintenance et le développement de CREODIAS, indépendamment du projet DAS (CLOUDFERRO a répondu à l'appel d'offres).

#### **[MUNDI WEB SERVICES](https://mundiwebservices.com/)**

|     |     |     |
| --- | --- | --- |
| **Documentation** ⬤⬤⬤◯◯<br/> Documentation bien organisée, mais manquant de matière et pas très agréable à parcourir | **Echanges** ⬤⬤⬤⬤⬤<br/>Très bonne réactivité et réponses pertinentes | **Performances** ⬤⬤⬤⬤⬤<br/>Parmi les meilleures performances internes pour l&#39;accès aux données. Performances externes correctes. |
| **Volume à chaud** ⬤⬤⬤◯◯<br/> 8 Petabytes | **Types de données** ⬤⬤⬤◯◯<br/> Copernicus + Landsat 8 + partenariat pour données météo et très haute résolution (Digital Globe) | **Données calculées** ⬤⬤⬤⬤⬤<br/>Nombreux jeux de données calculés : NDVI, NDWI, SWIR, Bathymétrie... |
| **Interface** ⬤⬤⬤⬤⬤<br/>Interface facile d&#39;utilisation et intuitive | **APIs** ⬤⬤⬤⬤◯<br/> Services OGC riches ( types de données, projections, accès par bande…). Quelques défauts pratiques : pas d&#39;accès à un produit via WMS, URL de téléchargement d&#39;un produit pas évidente à composer | **Fonctionnalités** ⬤⬤⬤⬤⬤<br/> Service payant de fourniture d&#39;imagerie sous la forme de COG (Cloud Optimized GeoTiff) pour données \&lt; 3 mois. Services OGC WPS et pipeline de calcul. API Offline Data Request pour demander automatiquement des données "froides" |

**En résumé :**

Plateforme très performante et pensée pour être pratique, avec le souci de proposer plus que le minimum (services OGC bien pensés, seule plateforme à proposer des services de calcul, données calculées...).

Le volume à chaud était son point faible en 2019 mais, avec la réduction du volume à chaud chez la plupart des plateformes, il devient plutôt bon.

Son premier point faible est son catalogue de données, moins étoffé que ses concurrents. De plus, l&#39;ancrage moins fort d&#39;Atos, pilote du projet, dans le monde du spatial n&#39;aide pas à les rendre visibles.

Beaucoup de ses utilisateurs ne font que tester la plateforme et accéder ponctuellement à des données. Peu de projets utilisent la plateforme.

La plateforme devrait vraisemblablement continuer d'exister jusqu'à l'arrivée de la future plateforme DAS. ATOS a répondu à l'appel d'offres DAS.

#### **[ONDA](https://www.onda-dias.eu)**

|     |     |     |
| --- | --- | --- |
| **Documentation** ⬤⬤⬤⬤◯<br/>Documentation très bien écrite et organisée, sensiblement améliorée depuis 2019 | **Echanges** ⬤⬤⬤⬤⬤<br/>Très bonne réactivité | **Performances** ⬤⬤⬤⬤◯<br/>Excellentes performances internes pour l&#39;accès aux données (les meilleures). Performances externes correctes. |
| **Volume à chaud** ⬤⬤◯◯◯<br/>Volume à chaud correspondant à seulement 1 mois de données (844 TB), mais une API permet d&#39;obtenir tous les jeux de données en 10 minutes | **Types de données** ⬤⬤⬤⬤◯<br/>Copernicus + Envisat + Landsat 8 + KOMPSAT + Deimos-2 + Maxar | **Données calculées** ◯◯◯◯◯<br/> Aucune donnée |
| **Interface** ⬤⬤⬤⬤⬤<br/>Très facile d&#39;utilisation | **APIs** ⬤⬤⬤⬤◯<br/>API un peu difficile à comprendre au premier abord, avec quelques services web peu logiques. API OGC peu claire | **Fonctionnalités** ⬤⬤⬤⬤◯<br/>Service [ONDA](https://www.onda-dias.eu) Data Relay pour s&#39;abonner à un services de fournitures d&#39;imagerie à la demande. Solution Data Hosting Services pour servir ses propres données image |

**En résumé :**

Plateforme dans l&#39;ensemble bien conçue et documentée, malgré des API peu claires au premier abord, avec un support et des performances internes excellentes.

Alors qu&#39;[ONDA](https://www.onda-dias.eu) proposait l&#39;ensemble du catalogue Copernicus en 2019, sa politique de gestion des données à chaud/froid est devenue un point faible, avec seulement 1 mois de données à chaud et un délai annoncé de dix minutes pour les données plus anciennes. [ONDA](https://www.onda-dias.eu) précise néanmoins avoir mis en place cette politique de gestion de données en concertation avec sa clientèle.

Tout comme les autres plateformes, ses utilisateurs sont essentiellement issus du monde universitaire et de la recherche.

80% des utilisateurs poursuivent leur contrat au bout d&#39;un an.

90% des clients payants sont des PME/Startups, mais ne représentent que 10% des revenus. 90% des revenus viennent de contrats publics (appels d&#39;offre sur 3-5 ans) remportés par [ONDA](https://www.onda-dias.eu).

[ONDA](https://www.onda-dias.eu) cherche à devenir un &quot;broker&quot; de données satellitaires, dans le but de simplifier/uniformiser l&#39;accès à de nombreuses plateformes ainsi que la facturation associée.

La plateforme devrait vraisemblablement continuer d'exister pour ses clients actuels. SERCO a répondu à l'appel d'offres DAS.

#### **[SOBLOO](https://sobloo.eu/)**

|     |     |     |
| --- | --- | --- |
| **Documentation** ⬤⬤⬤⬤◯<br/>Documentation très bien écrite, avec de nombreux exemples bien expliqués.Le wiki gitlab ne facilite néanmoins pas la recherche d&#39;information (pas de moteur de recherche) | **Echanges** ⬤⬤⬤⬤◯<br/>Réactivité un peu moins bonne que certaines plateformes, mais réponses pertinentes et efficaces | **Performances** ⬤⬤⬤⬤◯<br/>Performances internes moyennes, et performances externes les meilleures. |
| **Volume à chaud** ⬤⬤⬤◯◯<br/>6-7 PB. API de Data Warming | **Types de données** ⬤⬤⬤⬤⬤<br/> Copernicus + Spot 6/7 + Pleiades + Muscate + soCAP + Flux Vision Orange | **Données calculées** ◯◯◯◯◯<br/> Aucune donnée |
| **Interface** ⬤⬤⬤◯◯<br/>Interface peu intuitive et pratique | **APIs** ⬤⬤⬤◯◯<br/>Les API de catalogage et téléchargement sont faciles d&#39;utilisation. Les API OGC semblent avoir été réalisées par obligation, et ne proposent rien d&#39;intéressant | **Fonctionnalités** ⬤⬤⬤◯◯<br/>API de Data Warming. Cloud Orange implanté sur les cinq contients (70 datcenters). Solution pour servir ses propres données image |

**En résumé :**

Plateforme bien conçue et documentée, avec un support efficace. Elle se différencie par ses types de données et la richesse fonctionnelle du cloud Orange.

Ses performances sont moyennes. Son point faible est de se cantonner au minimum du cahier des charges ESA.

[SOBLOO](https://sobloo.eu/) dit avoir des difficultés à convaincre des PME/Startup de devenir clientes. Par ailleurs, l&#39;offre cloud basée sur une tarification fine et évolutive n&#39;est pas adaptée à des clients gérant leur budget à l&#39;année. [SOBLOO](https://sobloo.eu/) travaille à une tarification adaptée à cette clientèle.

La plateforme devrait vraisemblablement continuer d'exister jusqu'à l'arrivée de la future plateforme DAS. AIRBUS n'a pas répondu à l'appel d'offres DAS et cherche à développer une offre de Data Broking mondiale sous le nom ExtenSO.

#### **[WEKEO](https://www.wekeo.eu/)**

|     |     |     |
| --- | --- | --- |
| **Documentation** ⬤⬤⬤⬤◯<br/>Documentation claire. | **Echanges** ⬤◯◯◯◯<br/>Faible réactivité et traitement peu efficace des questions | **Performances**<br/>Nous n&#39;avons pas pu essayer les API S3 et NFS proposées seulement aux utilisateurs commerciaux |
| **Volume à chaud** ⬤⬤◯◯◯<br/>4.7 Petabytes en 2019 | **Types de données** ⬤⬤⬤◯◯<br/>Copernicus | **Données calculées** ◯◯◯◯◯<br/>Aucune donnée |
 | **Interface** ⬤⬤◯◯◯<br/>Interface de prévisualisation cartographique | **APIs** ⬤⬤◯◯◯<br/>Pas d&#39;API OGC WMS/WMTS avant fin 2020 | **Fonctionnalités** ⬤⬤◯◯◯<br/>Rien de plus que le cahier des charges |

**En résumé :**

Plateforme se disant identique aux autres, mais ayant clairement bénéficié d&#39;un cahier des charges allégé ou d&#39;échéances décalées dans le temps (pas de services OGC avant fin 2020, pas de politique tarifaire avant 2020).

Les performances d&#39;accès aux données à l&#39;intérieur du cloud sont difficiles à évaluer car les API S3 et NFS ne sont proposées qu&#39;aux utilisateurs commerciaux. Les performances de l&#39;API par défaut sont dix fois moins bonnes que les meilleurs temps moyens constatés chez deux concurrents.

[WEKEO](https://www.wekeo.eu/) ne propose que les données Copernicus, et il reste difficile en 2020 d&#39;obtenir beaucoup d&#39;informations sur ce qu&#39;elle propose.

Un helpdesk a été mis en place pour répondre aux questions mais, malgré plusieurs échanges, l&#39;interlocuteur n&#39;a pas réussi à obtenir des informations telles que le volume de données disponibles à chaud.

[WEKEO](https://www.wekeo.eu/) se spécialise sur les besoins autour de l&#39;océan et l&#39;atmosphère, et revendique une clientèle aux ⅔ issue du monde de la recherche.

Comme en 2019, il n&#39;a pas été possible d&#39;organiser un échange avec les responsables de la plateforme, en charge d&#39;autres projets au sein de leur société.

​

## ​6.​ Conclusion

**Cheminement**

Après 4 mois de travail sur cette étude en 2019, 2 mois en 2020 puis 1 mois en 2022, notre point de vue sur le projet DIAS a connu des hauts et des bas.

Au départ, nous avons été particulièrement peu convaincus par la démarche de financement de cinq consortiums concurrents par le biais de mécanismes de subvention, que nous avons appréhendés dans notre passé professionnel et dont nous n&#39;avons d'expérience que très rarement vu les effets bénéfiques pour le commanditaire. Bon nombre de ces projets aboutissent à des réalisations inabouties, inutilisables et servent discrètement à financer les roadmaps R&D de chaque société.

Notre point de vue était donc teinté d&#39;une certaine méfiance à l&#39;égard des services vantés par chaque plateforme DIAS, et de doutes sur la pérennité de celles-ci une fois les dernières subventions versées. Ce doute ne nous a pas quittés et, pour tous les acteurs du domain et usagers de données spatiales, les incertitudes sur la solidité financière/pérennité des DIAS ont nui à leur développement.

L'annonce de la fin des DIAS en 2022, remplacée par une seule et même plateforme européenne DAS à partir de 2023, n'a surpris personne dans le milieu. Il n'est pas non plus surprenant que les consortiums ayant répondu à l'appel d'offres pour cette future plateforme soient composés des mêmes sociétés qui avaient conçu les DIAS. 

Pour nuancer cette déception, il est néanmoins important de souligner que nous avons rencontré les représentants de chaque plateforme, testé leurs services et nous nous sommes rendus compte que, de manière générale, l&#39;offre est réelle et souvent de qualité. De fortes disparités et nuances existent en effet entre les plateformes, et cette étude essaie autant que possible de les mettre en lumière.

**En résumé : les DIAS sont (presque) morts, vive les DIAS !**

Ce que nous retenons particulièrement de cette étude, c&#39;est l&#39;expérience qu'a constituté l'initiative DIAS dans l&#39;écosystème de diffusion des données du programme Copernicus.

Auparavant entre les mains d&#39;organismes publics, du monde académique ou grands acteur américains du Cloud, la diffusion des données Copernicus a pu être proposée par des organismes privés européens dont les services et priorités sont orientés client, avec ce qui en découle : une compétition pour proposer les meilleurs services (fonctionnalités, contenu, performances, fiabilité…) aux meilleurs coûts. A condition qu&#39;une réelle concurrence s&#39;établisse dans le temps, cela devrait être bénéfique pour tous les utilisateurs.

L&#39;offre proposée par les DIAS a surtout eu pour plus-value, par rapport à l&#39;existant, de permettre un accès très performant aux données depuis le cloud de chaque plateforme (ce que le secteur recherche/académique ne proposait pas), et des mécanismes basés sur des protocoles ouverts (OpenSearch, OGC) pour découvrir les données.

En revanche, l&#39;offre en termes de calcul est restée très pauvre : difficile par nature à définir car devant répondre à des besoins très variés, les plateformes (et l'ESA) n'ont pas réussi à apporter une réelle plus-value dans le domaine.
Sauf de réelles exceptions, que nous encourageons le lecteur à considérer, l&#39;offre cloud des plateformes n&#39;a eu pour ainsi dire d&#39;intéressant, par rapport à d&#39;autres offres cloud, que la possibilité d&#39;accéder de façon très performante aux données.

**Data Access Service ?**

L&#39;es plateformes DIAS, et le cahier des charges de l'ESA sur lequel elles devaient s'appuyer, n'ont pas réussi à trouver un modèle économique viable et à installer une offre européenne à même de concurrence les géants du Cloud américains.

La future plateforme DAS ("Data Access Service") de l'ESA a vraisemblablement l'ambition de proposer une solution européenne de référence pour l'accès performant à l'ensemble des données du programme Copernicus. Cette plateforme suscite de nombreuses interrogations :
  * Saura-t-elle proposer une offre claire, facile à exploiter, avec un accompagnement des usagers potentiels ?
  * Proposera-t-elle l'ensemble du catalogue Copernicus sans délais de téléchargement ?
  * Comment répondra-t-elle aux attentes d'usagers répartis sur tout la planète, nécessitant sans doute des réplicats locaux pour bénéficier de bonnes performances de téléchargement ?
  * Proposera-t-elle plus que le catalogue de produits bruts Copernicus, comme des données calculées ?
  * Que proposera-t-elle aux usagers nécessitant d'être avertis ("triggers") dès qu'une donnée est disponible ?

De premières réponses seront apportées dès 2023 avec la première version de la plateforme.

En parallèle, les sociétés ayant contribués aux DIAS vont aussi pour certaines développer des offres concurrentes ou complémentaires : 
  - CLOUDFERRO, qui va continuer d'opérer le DIAS CREODIAS (tout en répondant à l'appel d'offres DAS)
  - AIRBUS, qui se positionne en tant que Data Broker avec son offre ExtenSO (et ne s'est pas positionné sur l'appel d'offres DAS)
  - ATOS, qui poursuit le développement de son activité dans le spatial avec notamment la participation à un projet de jumeau numérique européen  (et a répondu à l'appel d'offres DAS)
  - SERCO, qui va continuer de maintenir le DIAS ONDA pour ses clients actuels (et a répondu à l'appel d'offres DAS)

Nous espérons que cette étude aura permis d'éclairer divers acteurs du domaine spatial ou usagers sur les enjeux de ces dernières années en Europe autour de la diffusion des données Copernicus.
Alors que les plateformes de diffusion de données spatiales se développent partout dans le monde, sous la forme d'entrepôts de données performants (comme DAS, AWS, Azure...) ou de solutions de Data Broking, nous faisons le pari que de telles études comparatives vont à l'avenir être plus que nécessaires pour orienter les utilisateurs et leur permettre de construire des solutions/offres tirant judicieusement parti de l'écosystème mondial.
